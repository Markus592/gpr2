<?php require(__DIR__ . "/../include/config.php"); ?>
<?php
define('og_image', 'images/novedades/aleli-11.jpg');
define('og_title', 'TU OPORTUNIDAD PARA TENER UNA VIVIENDA ECOLÓGICA A BAJO COSTO.');
define('og_type', 'website');
define('og_desc', 'La forma de vida de nuestros pueblos originarios se caracterizó por una convivencia armónica con la naturaleza. Con el pasar del tiempo –y hablamos de cientos de años- las sociedades comenzaron a transformarse, dejando a un lado esa especial relación de respeto hacia la madre tierra. Se volvieron más industrializadas y menos sensibles a su entorno, generando trastornos y cambios drásticos en el planeta. ');
define('keywords', ',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
define('title', GPR_TITLE_NOVEDADES . og_title);
define('GPR_ACTUAL_URL', GPR_ROOT_PATH . "novedades/tu-oportunidad-para-tener-una-vivienda-ecologica-a-bajo-costo.php");
define('GPR_SECTION_CLASS', 'novedades11');
define('GPR_FECHA_NOVEDAD', '21 de Setiembre 2020');
?>
<?php require(__DIR__ . "/../include/header.php"); ?>


<section id="agent-page" class="header-margin-base fixed-no-header page-blog">

	<div class="hero-page">
		<div class="info-hero">
			<h1 class="title-name name">¡Lo que aún no sabes sobre el “Bono Sostenible del FMV”!</h1>
			<div class="info-name cotizar-btn">
				<span class="title">Cotiza ahora tu casa</span>
				<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>" alt="arrow-up" /></a>
			</div>
		</div>
	</div>
	<?php include "../include/block_fecha.php" ?>
	<div class="container">
		<div class="row indice-content">
			<div class="col-md-12">
				<p><?php include "../include/sharebutton.php" ?></p>
				<div class="section-title">
					<h2 class="title title-grand">Indice de Contenidos</h2>
				</div>
				<li><a href="#indice1">Tu oportunidad para tener una vivienda ecol&oacute;gica a bajo costo.</a></li>
				<li><a href="#indice2">Buenas noticias </a></li>
				<li><a href="#indice3">¿Qu&eacute; significa “Bono Sostenible”?</a></li>
				<li><a href="#indice4">¿En Arequipa existen construcciones de “viviendas sostenibles”?</a></li>
				<li><a href="#indice5">¿Sab&iacute;as que en este momento tienes a tu favor todos los factores que facilitan la compra de tu vivienda, y adem&aacute;s ecol&oacute;gica? </a></li>
				<li><a href="#indice6">¿Deseas mayor informaci&oacute;n?</a></li>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-sm-12 col-md-12">

						<div id="indice1" class="section-title">
							<h2 class="title">Tu oportunidad para tener una vivienda ecol&oacute;gica a bajo costo</h2>
						</div>
						<p>La forma de vida de nuestros pueblos originarios se caracteriz&oacute; por una convivencia arm&oacute;nica con la naturaleza. Con el pasar del tiempo –y hablamos de cientos de años- las sociedades comenzaron a transformarse, dejando a un lado esa especial relaci&oacute;n de respeto hacia la madre tierra. Se volvieron m&aacute;s industrializadas y menos sensibles a su entorno, generando trastornos y cambios dr&aacute;sticos en el planeta. </p>
						<p>Como una alerta, en las últimas d&eacute;cadas se iniciaron grandes movimientos humanos que han exigido la implementaci&oacute;n de medidas sanas que contribuyan a salvaguardar el medio ambiente, aplicable a todos los &aacute;mbitos de nuestro quehacer diario, <b>y la construcci&oacute;n de espacios habitacionales sustentables</b> no escapa de ellas. </p>
						<div id="indice2" class="section-title">
							<h2 class="title">Buenas noticias </h2>
						</div>
						<p><b>GPR Inmobiliaria</b> ha dado un paso al frente en la construcci&oacute;n de <b>viviendas,</b> no solo <b>econ&oacute;micas</b> o de bajo costo, sino tambi&eacute;n <b>sustentables,</b> destinadas a personas de menos recursos o de grupos sociales m&aacute;s vulnerables.</p>
						<p>A trav&eacute;s del Fondo Mi Vivienda, todos los ciudadanos que califiquen podr&aacute;n optar al <b>Bono Sostenible,</b> que les permitir&aacute; adquirir inmuebles con caracter&iacute;stica especiales, “<i>… que ayuden a la reducci&oacute;n del impacto ambiental en su entorno</i>”.</p>
						<div id="indice3" class="section-title">
							<h2 class="title">¿Qu&eacute; significa “<i>Bono Sostenible</i>”?</h2>
						</div>
						<p>Es simple. El Bono Sostenible es una “<i>ayuda econ&oacute;mica que premia el esfuerzo de las personas que acceden al programa Nuevo Cr&eacute;dito Mi Vivienda, pero que adem&aacute;s ofrece una tasa preferencial en todos los rangos, según el Grado de Sostenibilidad de la vivienda”</i>.</p>
						<p>A manera de explicaci&oacute;n, el Bono Sostenible es el resultado de la suma del Bono del Buen Pagador (que var&iacute;a de acuerdo al precio de la vivienda que se desea comprar) y un Bono único adicional de S/ 5.000, que se asigna al comprador cuando la vivienda que va a adquirir cumple con criterios de <b>sostenibilidad</b>, es decir, que permite el ahorro de agua, de energ&iacute;a lum&iacute;nica y calentamiento de agua, un plan de manejo de residuos y reciclaje en la construcci&oacute;n, as&iacute; como otros aspectos de respeto hacia nuestro h&aacute;bitat. </p>
						<p>Adicionalmente, el comprador tambi&eacute;n cuenta con tasas preferenciales fijas, que permiten tener cuotas de pago sustancialmente bajas y estables.</p>
						<div id="indice4" class="section-title">
							<h2 class="title">¿En Arequipa existen construcciones de “viviendas sostenibles”? </h2>
						</div>
						<p>S&iacute;, por supuesto. De hecho, recientemente <b>GPR Inmobiliaria obtuvo la Certificaci&oacute;n Nº 003_C-2020</b> emitida por <b>AENOR</b> (Asociaci&oacute;n Española de Normalizaci&oacute;n y Certificaci&oacute;n) que avala como <b>Proyecto Sostenible</b> el desarrollo habitacional <b>Las Lomas de Yura</b>. En estos momentos se encuentra en la fase de su <b>1era. Etapa</b>, la cual avanza r&aacute;pidamente con la <b>entrega</b> programada para mediados del mes de febrero de 2021, de <b>244 casas</b> y <b>40 departamentos.</b></p>
						<div id="indice5" class="section-title">
							<h2 class="title">¿Sab&iacute;as que en este momento tienes a tu favor todos los factores que facilitan la compra de tu vivienda, y adem&aacute;s ecol&oacute;gica?</h2>
						</div>
						<p>Sorpr&eacute;ndete, pues s&oacute;lo hasta el 31 de diciembre de 2020 podr&aacute;s optar a todas estas facilidades de forma conjunta:</p>
						<ul>
							<li>El Fondo Mi Vivienda te ofrece excelentes beneficios a trav&eacute;s del otorgamiento del Bono Sostenible (Bono Buen Pagador + S/ 5.000 por concepto de sostenibilidad).</li>
							<li>Los Bancos financian tu compra a 20 años, y te ofrecen cuotas preferenciales fijas a un 6% de inter&eacute;s.</li>
							<li>Solo tienes que disponer entre el 7.5% y 10% de inicial del inmueble que deseas comprar (dependiendo del Banco donde se gestione el cr&eacute;dito).</li>
							<li>Con GPR Inmobiliaria y el proyecto habitacional <b>Las Lomas de Yura</b> en Arequipa, tienes la oportunidad de adquirir un hogar que <b>contribuye</b> no solo con la <b>sostenibilidad y el medio ambiente</b>, sino que tambi&eacute;n <b>te permite ahorrar un porcentaje en el uso del agua y la energ&iacute;a</b>, que se ver&aacute;n reflejados en la <b>disminuci&oacute;n</b> considerable de tus <b>gastos</b> a futuro con el uso de estos servicios.</li>
							<li>Est&aacute;s adquiriendo una <b>vivienda</b> que cumple con est&aacute;ndares de <b>calidad</b> y protecci&oacute;n medioambiental, <b>reconocidos</b> tanto a nivel <b>nacional</b> como <b>internacional.</b></li>
						</ul>
						<div id="indice6" class="section-title">
							<h2 class="title">¿Deseas mayor informaci&oacute;n?</h2>
						</div>
						<p>GPR Inmobiliaria dispone de tres oficinas en Arequipa. All&iacute; te recibiremos con agrado. Consúltanos tus dudas, no dejes pasar esta oportunidad. </p>
						<p>Nuestros promotores de venta est&aacute;n en la capacidad de ayudarte en todo lo que necesites. Contáctanos:</p>
						<ul>
							<li>Maria : <b> <a href="tel:+51 923 104 967">+51 923 104 967</a></b>
							</li>
							<li>Daniel : <b> <a href="tel:+51 947 327 082">+51 947 327 082</a></b>
							</li>
							<li>Lidia : <b> <a href="tel:+51 947 326 649">+51 947 326 649</a></b>
							</li>
						</ul>
						<div class="cotizar-btn">
							<span class="title">Cotiza ahora tu casa</span>
							<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>" alt="arrow-up" /></a>
						</div>
					</div><!-- /.col-md-12 -->
				</div><!-- /.col-md-12 -->
			</div><!-- /.row -->
		</div>

	</div><!-- ./row -->
	<br /><br /><br />
	<div id=cotizar class="cotizar-vivienda">
		<div class="container">
			<div class="row">
				<div class="avanze1 col-sm-4 col-md-3">
					<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div>
				<div class="col-sm-12 col-md-9">
					<!-- ===================== 
												SEARCH 
										====================== -->
					<div class="line-style no-margin">
						<h2 class="title-grand">Cotizar Vivienda</h2>
					</div>
					<div class="right-box no-margin">
						<div class="row">
							<?php require(__DIR__ . "/../include/form-cotizar.php"); ?>
						</div><!-- ./row 2 -->
					</div><!-- ./search -->

				</div>
				<div class="avanze2 col-sm-4 col-md-3">
					<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div><!-- /.col-md-12 -->
			</div>
			<!--row-->
		</div>
	</div>
	<br /><br /><br />
	<div class="container">
		<div class="section-title line-style no-margin">
			<h2 class="title">Elige tu nuevo hogar</h2>
		</div>

		<div class="my-property" data-navigation=".my-property-nav">
			<div class="crsl-wrap">
				<?php require(__DIR__ . "/../include/grid-propiedades.php"); ?>
			</div>
			<div class="my-property-nav">
				<p class="button-container">
					<a href="#" class="next">siguiente</a>
					<a href="#" class="previous">anterior</a>
				</p>
			</div>
		</div><!-- /.my-property slide -->

	</div><!-- ./container -->
</section><!-- /#about-us -->




<?php require(__DIR__ . "/../include/footer2.php"); ?>