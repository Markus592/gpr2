<?php require(__DIR__ . "/../include/config.php"); ?>
<?php
define('og_image', 'images/novedades/aleli-11.jpg');
define('og_title', 'VIVIENDAS ECONÓMICAS vs. VIVIENDAS BARATAS');
define('og_type', 'website');
define('og_desc', 'El tema de la desigualdad puede parecernos incomprensible, especialmente cuando vemos que en otros ámbitos existe un mundo lleno de posibilidades. Y aunque eliminarla de forma inmediata es imposible, constantemente surgen propuestas que permiten hacer transformaciones valiosas y profundas en las personas, mejorando su calidad de vida y su futuro.');
define('keywords', ',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
define('title', GPR_TITLE_NOVEDADES . og_title);
define('GPR_ACTUAL_URL', GPR_ROOT_PATH . "novedades/viviendas-economicas-vs-viviendas-baratas.php");
define('GPR_SECTION_CLASS', 'novedades12');
define('GPR_FECHA_NOVEDAD', '17 de Setiembre 2020');
?>
<?php require(__DIR__ . "/../include/header.php"); ?>


<section id="agent-page" class="header-margin-base fixed-no-header page-blog">

	<div class="hero-page">
		<div class="info-hero">
			<h1 class="title-name name">VIVIENDAS ECONÓMICAS vs. VIVIENDAS BARATAS</h1>
			<div class="info-name cotizar-btn">
				<span class="title">Cotiza ahora tu casa</span>
				<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>" alt="arrow-up" /></a>
			</div>
		</div>
	</div>
	<?php include "../include/block_fecha.php" ?>
	<div class="container">
		<div class="row indice-content">
			<div class="col-md-12">
				<p><?php include "../include/sharebutton.php" ?></p>
				<div class="section-title">
					<h2 class="title title-grand">Indice de Contenidos</h2>
				</div>
				<li><a href="#indice1">¡Descubrir la diferencia es la mejor alternativa!</a></li>
				<li><a href="#indice2">La diferencia</a></li>
				<li><a href="#indice3">Nuevos desarrollos habitacionales en Arequipa: la respuesta favorable en manos de GPR INMOBILIARIA</a></li>
				<li><a href="#indice4">¿Deseas descubrir nuestra mayor propuesta inmobiliaria?</a></li>
				<li><a href="#indice5">¿Qué te parece si nos visitas?</a></li>
				<li><a href="#indice6">¿Prefieres quedarte en casa?</a></li>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div id="indice1" class="section-title">
							<h2 class="title">¡Descubrir la diferencia es la mejor alternativa!</h2>
						</div>
						<div class="bs-callout callout-info">
							<p class="text">El tema de la desigualdad puede parecernos incomprensible, especialmente cuando vemos que en otros ámbitos existe un mundo lleno de posibilidades. Y aunque eliminarla de forma inmediata es imposible, constantemente surgen <b><i>propuestas que permiten hacer transformaciones valiosas y profundas en las personas, mejorando su calidad de vida y su futuro.</i></b></p>
							<p></p>
							<p>Al respecto, las mayores diferencias pueden verse reflejada en aquellos que carecen de techo propio, que viven en condiciones de hacinamiento o en edificaciones ilegales y de alto riesgo. Es por esto que, a lo largo de los años, han surgido innumerables <b>proyectos de construcción de viviendas de interés social,</b> financiados en algunos casos por el Gobierno de cada país, y en otros por empresas privadas que plantean que su objetivo principal es <b><i>favorecer a grupos vulnerables o desprotegidos, para brindarles la oportunidad de acceder a un nuevo hogar.</i></b></p>
							<p>Y es que la necesidad de vivienda a bajo costo es un asunto social de especial importancia en estos tiempos, que tiene como finalidad dar respuesta a muchas familias de bajos ingresos, que tienen años viviendo en alquileres y necesitan mudarse. Esta situación ha ido aumentando con los años, pues también se agregan aquellos ciudadanos o grupos familiares que pierden sus casas debido a desastres naturales, conflictos sociales o cambios generalizados. Como resultado, se hace evidente que cada día se suman más y más personas, familias y comunidades, que viven en lugares que empeoran o agravan su estado de salud y su garantía de vida. </p>
						</div>
						<div id="indice2" class="section-title">
							<h2 class="title">La diferencia</h2>
						</div>
						<p>Durante varios años han aparecido proyectos habitacionales dedicados a promover la construcción de viviendas baratas para la población, con ofertas que enamoran por su facilidad e ilusorios detalles de aspectos aparentemente maravillosos y únicos pero que, en realidad cuando miramos a fondo, nos damos cuenta que no todo es tan perfecto. <b><i>Las viviendas baratas no necesariamente significan o guardan relación con el concepto de habitables y de calidad,</i></b> ya que en muchos casos resultan especies de tugurios o asentamientos extremadamente mezquinos, sin sistemas de aguas servidas y otros servicios. Caso contrario, las <b><i>viviendas económicas son aquellas que se desarrollan en espacios con diseños inteligentes que posibilitan y amplían la calidad de vida de sus habitantes.</i></b> Son lugares planificados, pensados para su uso a largo plazo, en todas las etapas de la vida. La gran diferencia es que éstas viviendas cuentan con todos sus servicios básicos, que se construyen en terrenos de tamaño adecuado, en ambientes agradables; que son elaboradas con materiales que ofrecen seguridad y posibilidades de ampliación a futuro. Viviendas Económicas y Viviendas Baratas son ideas totalmente distantes, diferentes, que no guardan ninguna relación.</p>
						<div id="indice3" class="section-title">
							<h2 class="title">Nuevos desarrollos habitacionales en Arequipa: la respuesta favorable en manos de GPR INMOBILIARIA</h2>
						</div>
						<p>En nuestro país, y como una política del Estado peruano, <b><i>en estos momentos se promueve, potencia e impulsa en la región de Arequipa</i></b> la construcción de uno de los proyectos habitacionales más ambiciosos de la zona, con <b><i>viviendas de bajo costo, realmente apegadas al concepto de económicas, bajo la responsabilidad y garantía de GPR Inmobiliaria,</i></b> empresa con sentido de compromiso social, que tiene como objetivo favorecer a las clases sociales de menos recursos económicos y de mayor vulnerabilidad. </p>
						<p>En <b><i>GPR INMOBILIARIA</i></b> <b><i>nos distinguimos porque realmente creamos condiciones de vida para las personas.</i></b> Ofrecemos y fomentamos urbanismos integrados con inmuebles a bajo precio, asequibles al común de la población, pero favorables y sustentables en el tiempo. </p>
						<div id="indice4" class="section-title">
							<h2 class="title">¿Deseas descubrir nuestra mayor propuesta inmobiliaria?</h2>
						</div>
						<p>Se trata del proyecto residencial <b>Las Lomas de Yura</b>, ubicado en el Km 17 de la carretera Arequipa - Puno, sector Las Laderas, distrito de Yura. Estamos en pleno desarrollo de la <b>1era. Etapa</b> de construcción, que consta de 288 viviendas, distribuidas en 244 casas y 40 departamentos. </p>
						<div id="indice5" class="section-title">
							<h2 class="title">¿Qué te parece si nos visitas?</h2>
						</div>
						<p>Te invitamos a acercarte a cualquiera de <b><a href="<?= GPR_ROOT_PATH ?>oficinas/oficina-central">nuestras oficinas</a></b>, ubicadas en diversas zonas de la ciudad de Arequipa, donde te daremos atención personalizada, respetando los protocolos sanitarios correspondientes e implementados por el Gobierno nacional debido a la pandemia del COVID-19.</p>
						<div id="indice6" class="section-title">
							<h2 class="title">¿Prefieres quedarte en casa?</h2>
						</div>
						<p>También es válido. Puedes comunicarte con nuestros Asesores de Ventas, y solicitar una cotización. Ellos están preparados para ayudarte, guiarte y responder a todas tus inquietudes. Sus teléfonos de contacto:</p>
						<ul>
							<li>Maria : <b> <a href="tel:+51 923 104 967">+51 923 104 967</a></b>
							</li>
							<li>Daniel : <b> <a href="tel:+51 947 327 082">+51 947 327 082</a></b>
							</li>
							<li>Lidia : <b> <a href="tel:+51 947 326 649">+51 947 326 649</a></b>
							</li>
						</ul>
						<div class="cotizar-btn">
							<span class="title">Cotiza ahora tu casa</span>
							<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>" alt="arrow-up" /></a>
						</div>
					</div><!-- /.col-md-12 -->
				</div><!-- /.col-md-12 -->
			</div><!-- /.row -->
		</div>

	</div><!-- ./row -->
	<br /><br /><br />
	<div id=cotizar class="cotizar-vivienda">
		<div class="container">
			<div class="row">
				<div class="avanze1 col-sm-4 col-md-3">
					<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div>
				<div class="col-sm-12 col-md-9">
					<!-- ===================== 
												SEARCH 
										====================== -->
					<div class="line-style no-margin">
						<h2 class="title-grand">Cotizar Vivienda</h2>
					</div>
					<div class="right-box no-margin">
						<div class="row">
							<?php require(__DIR__ . "/../include/form-cotizar.php"); ?>
						</div><!-- ./row 2 -->
					</div><!-- ./search -->

				</div>
				<div class="avanze2 col-sm-4 col-md-3">
					<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div><!-- /.col-md-12 -->
			</div>
			<!--row-->
		</div>
	</div>
	<br /><br /><br />
	<div class="container">
		<div class="section-title line-style no-margin">
			<h2 class="title">Elige tu nuevo hogar</h2>
		</div>

		<div class="my-property" data-navigation=".my-property-nav">
			<div class="crsl-wrap">
				<?php require(__DIR__ . "/../include/grid-propiedades.php"); ?>
			</div>
			<div class="my-property-nav">
				<p class="button-container">
					<a href="#" class="next">siguiente</a>
					<a href="#" class="previous">anterior</a>
				</p>
			</div>
		</div><!-- /.my-property slide -->

	</div><!-- ./container -->
</section><!-- /#about-us -->




<?php require(__DIR__ . "/../include/footer2.php"); ?>