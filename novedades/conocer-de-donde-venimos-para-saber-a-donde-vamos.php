<?php require(__DIR__ . "/../include/config.php"); ?>
<?php
define('og_image', 'images/novedades/aleli-11.jpg');
define('og_title', '¡Conocer de dónde venimos, para saber a dónde vamos!');
define('og_type', 'website');
define('og_desc', 'La historia de los primeros habitantes de Arequipa está llena de hermosos mitos que nos enseñan de dónde venimos, cuales son nuestros orígenes, pero que además nos recuerdan que llevamos en nuestra sangre una de las herencias culturales más importantes de América: La Inca.');
define('keywords', ',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
define('title', GPR_TITLE_NOVEDADES . og_title);
define('GPR_ACTUAL_URL', GPR_ROOT_PATH . "novedades/conocer-de-donde-venimos-para-saber-a-donde-vamos.php");
define('GPR_SECTION_CLASS', 'novedades10');
define('GPR_FECHA_NOVEDAD', '25 de Setiembre 2020');
?>
<?php require(__DIR__ . "/../include/header.php"); ?>


<section id="agent-page" class="header-margin-base fixed-no-header page-blog">
	<div class="hero-page">
		<div class="info-hero">
			<h1 class="title-name name">¡Conocer de dónde venimos, para saber a dónde vamos!</h1>
			<div class="info-name cotizar-btn">
				<span class="title">Cotiza ahora tu casa</span>
				<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>" alt="arrow-up" /></a>
			</div>
		</div>
	</div>
	<?php include "../include/block_fecha.php" ?>
	<div class="container">
		<div class="row indice-content">
			<div class="col-md-12">
				<p><?php include "../include/sharebutton.php" ?></p>
				<div class="bs-callout callout-info">
					<h4 class="title">DEL SILLAR BLANCO A LAS LOMAS DE YURA</h4>
					<p class="text">La historia de los primeros habitantes de Arequipa est&aacute; llena de hermosos mitos que nos enseñan <b>de dónde venimos</b>, cuales son nuestros orígenes, pero que adem&aacute;s nos recuerdan que llevamos en nuestra sangre una de las herencias culturales m&aacute;s importantes de Am&eacute;rica: La Inca.</p>
				</div>
				<p>Entre tantas leyendas que constantemente escuchamos, una de las m&aacute;s conocidas est&aacute; dedicada al gobernante <i>Mayta Qhapaq</i>. Cuentan que, al pasar por estas tierras acompañado por su ej&eacute;rcito, algunos de sus hombres le pidieron permiso para quedarse, pues estaban impactados por el paisaje de exuberante belleza de esta campiña. Fue entonces cuando <i>Mayta</i> les respondió <i>“Ariqquepay”</i>, en quechua, que significa en español “Sí, <i>qu&eacute;dense</i>”. Dicen, entre otras versiones, que de allí se origina el nombre de <i>Arequipa</i>.</p>
				<p>Pero es que, en Arequipa, adem&aacute;s de esta tradición oral de mitos y leyendas, transmitida de generación en generación, tambi&eacute;n existen significativos símbolos que nos identifican como pueblo y de los cuales nos sentimos plenamente orgullosos, como los volcanes que nos custodian, la gastronomía tradicional, las edificaciones coloniales, la flora, la fauna, los bailes y las danzas típicas, la música, entre muchos otros elementos. Asimismo, es común escuchar los calificativos que la exaltan y la refieren como “<i>la ciudad blanca</i>”, “<i>la Roma de Am&eacute;rica</i>” o simplemente “<i>Arequipa, eterna primavera</i>”.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-sm-12 col-md-12">




						<div id="indice1" class="section-title">
							<h2 class="title">No permitir el olvido y saber a dónde vamos</h2>
						</div>
						<p>Con el pasar de los años vinieron las transformaciones culturales, sociales y económicas originadas, entre otros factores, por el aumento de la población y la movilización territorial de grupos humanos. Simult&aacute;neamente a los cambios, comenzaron a generarse políticas públicas que permitieron dar respuesta a este crecimiento y a la adaptación de la nueva realidad.</p>
						<p>En el caso de Arequipa vemos, por ejemplo, como en el tema de las edificaciones, pasamos de los sistemas constructivos cl&aacute;sicos, basados en el uso del “sillar blanco”, a otros con características diferentes adecuadas a las necesidades de sus habitantes, al crecimiento de las comunidades y a los tiempos modernos. Es así que, en todo este proceso de evolución, avanzamos y llegamos a una propuesta novedosa de desarrollo habitacional: <b>Las Lomas de Yura.</b> </p>
						<p>Este mega proyecto se ha convertido en uno de los m&aacute;s importantes de la región porque, adem&aacute;s de <b>disponer de viviendas económicas y de bajo costo</b> para personas con menor poder adquisitivo, las mismas han sido pensadas para trascender generaciones como <b>espacios sostenibles que disminuyen el impacto</b> <b>medioambiental</b>, reduciendo el consumo de agua y de electricidad.</p>
						<p>Como valor añadido, y en tributo a nuestras raíces m&aacute;s profundas, que reafirman la identidad de los arequipeños, cada una de las viviendas en <b>Las Lomas de Yura</b> hace honor y guarda una vinculación estrecha con la simbología ancestral, característica de esta región: el volc&aacute;n Misti, el &aacute;rbol Capulí, la flor de Texao, la flor de Alelí, la danza Wititi y el canto Yarabí.</p>
						<ul>
							<li><a href="<?= GPR_ROOT_PATH ?>propiedad/casa-capuli"><b>Casa Capulí </b></a></li>
							<li><a href="<?= GPR_ROOT_PATH ?>propiedad/casa-texao"><b>Casa Texao</b></a></li>
							<li><a href="<?= GPR_ROOT_PATH ?>propiedad/casa-misti"><b>Casa Misti</b></a></li>
							<li><a href="<?= GPR_ROOT_PATH ?>propiedad/casa-aleli"><b>Casa Aleli</b></a></li>
							<li><a href="<?= GPR_ROOT_PATH ?>propiedad/departamento-wititi"><b>Dpto. Wititi</b></a></li>
							<li><a href="<?= GPR_ROOT_PATH ?>propiedad/departamento-yaravi"><b>Dpto. Yarabí</b></a></li>
						</ul>
						<p>Adem&aacute;s, puedes contactar a nuestros promotores de venta para recibir información adicional sobre las formas de financiamiento y las facilidades que te ofrece el Fondo Mi Vivienda, a trav&eacute;s del Nuevo Cr&eacute;dito Mi Vivienda, el programa Techo Propio y el BBP (Bono del Buen Pagador) Sostenible.</p>
						<p>No dejes pasar esta oportunidad. Ac&eacute;rcate a cualquiera de nuestras tres oficinas, o ll&aacute;manos a nuestros números telefónicos:</p>
						<ul>
							<li>Maria : <b> <a href="tel:+51 923 104 967">+51 923 104 967</a></b>
							</li>
							<li>Daniel : <b> <a href="tel:+51 947 327 082">+51 947 327 082</a></b>
							</li>
							<li>Lidia : <b> <a href="tel:+51 947 326 649">+51 947 326 649</a></b>
							</li>
						</ul>
						<div class="cotizar-btn">
							<span class="title">Cotiza ahora tu casa</span>
							<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>" alt="arrow-up" /></a>
						</div>
					</div><!-- /.col-md-12 -->
				</div><!-- /.col-md-12 -->
			</div><!-- /.row -->
		</div>

	</div><!-- ./row -->
	
	<br /><br /><br />
	<div id=cotizar class="cotizar-vivienda">
		<div class="container">
			<div class="row">
				<div class="avanze1 col-sm-4 col-md-3">
					<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div>
				<div class="col-sm-12 col-md-9">
					<!-- ===================== 
												SEARCH 
										====================== -->
					<div class="line-style no-margin">
						<h2 class="title-grand">Cotizar Vivienda</h2>
					</div>
					<div class="right-box no-margin">
						<div class="row">
							<?php require(__DIR__ . "/../include/form-cotizar.php"); ?>
						</div><!-- ./row 2 -->
					</div><!-- ./search -->

				</div>
				<div class="avanze2 col-sm-4 col-md-3">
					<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div><!-- /.col-md-12 -->
			</div>
			<!--row-->
		</div>
	</div>
	<br /><br /><br />
	<div class="container">
		<div class="section-title line-style no-margin">
			<h2 class="title">Elige tu nuevo hogar</h2>
		</div>

		<div class="my-property" data-navigation=".my-property-nav">
			<div class="crsl-wrap">
				<?php require(__DIR__ . "/../include/grid-propiedades.php"); ?>
			</div>
			<div class="my-property-nav">
				<p class="button-container">
					<a href="#" class="next">siguiente</a>
					<a href="#" class="previous">anterior</a>
				</p>
			</div>
		</div><!-- /.my-property slide -->

	</div><!-- ./container -->
</section><!-- /#about-us -->




<?php require(__DIR__ . "/../include/footer2.php"); ?>