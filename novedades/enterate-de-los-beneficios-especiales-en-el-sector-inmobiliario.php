<?php require(__DIR__ . "/../include/config.php"); ?>
<?php
define('og_image', 'images/novedades/aleli-11.jpg');
define('og_title', 'Entérate de los beneficios especiales en el sector Inmobiliario');
define('og_type', 'website');
define('og_desc', 'A pesar de tantas adversidades, los arequipeños nos distinguidos por ser incansables luchadores, dispuestos a enfrentar cualquier situación que nos desfavorezca, porque "no se nace en vano al pie de un volcán"');
define('keywords', ',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
define('title', GPR_TITLE_NOVEDADES . og_title);
define('GPR_ACTUAL_URL', GPR_ROOT_PATH . "novedades/enterate-de-los-beneficios-especiales-en-el-sector-inmobiliario.php");
define('GPR_SECTION_CLASS', 'novedades09');
define('GPR_FECHA_NOVEDAD', '29 de Setiembre 2020');
?>
<?php require(__DIR__ . "/../include/header.php"); ?>
<section id="agent-page" class="header-margin-base fixed-no-header page-blog">
	<div class="hero-page">
		<div class="info-hero">
			<h1 class="title-name name">¡ENT&Eacute;RATE DE LOS BENEFICIOS ESPECIALES EN EL SECTOR INMOBILIARIO!</h1>
			<div class="info-name cotizar-btn">
				<span class="title">Cotiza ahora tu casa</span>
				<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>" alt="arrow-up" /></a>
			</div>
		</div>
	</div>
	<?php include "../include/block_fecha.php" ?>
	<div class="container">
		<div class="row indice-content">
			<div class="col-md-12">
				<p><?php include "../include/sharebutton.php" ?></p>
				<p>A pesar de tantas adversidades, los arequipeños nos distinguidos por ser incansables luchadores, dispuestos a enfrentar cualquier situaci&oacute;n que nos desfavorezca, porque “no se nace en vano al pie de un volc&aacute;n”.</p>
				<p>El trabajo que hacemos diariamente para salir adelante, trae consigo logros y oportunidades, permiti&eacute;ndonos alcanzar objetivos que algunas veces ve&iacute;amos lejanos. En el caso de las oportunidades, dicen que se presentan una sola vez en la vida. Realmente no podemos afirmar que eso, de “una sola vez en la vida”, sea cierto, pero de lo que si estamos seguros es que, cuando se presentan, hay que tomarlas porque son el resultado de nuestro esfuerzo, y si guardan relaci&oacute;n con la compra de una vivienda ¡No hay que pensarlo mucho!</p>

				<div class="section-title">
					<h2 class="title title-grand">Indice de Contenidos</h2>
				</div>
				<li><a href="#indice1">Crecimiento inmobiliario en Arequipa</a></li>
				<li><a href="#indice2">Viviendas con caracter&iacute;sticas &uacute;nicas</a></li>
				<li><a href="#indice3">Tu oportunidad: Beneficios antes del 31 de diciembre de 2020</a></li>
				<li><a href="#indice4">¡No puedes dejar pasar este momento! ¡Post&uacute;late Ya!</a></li>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div id="indice1" class="section-title">
							<h2 class="title">Crecimiento inmobiliario en Arequipa</h2>
						</div>
						<p>En los &uacute;ltimos tiempos en Arequipa, se han puesto en marcha diversas ideas inmobiliarias que poseen caracter&iacute;sticas propias, dependiendo del sector al cual est&aacute;n dirigidas. Como resultado, se experiment&oacute; un crecimiento que tuvo su origen en el impulso y apoyo facilitados por el Estado peruano para la construcci&oacute;n de viviendas de inter&eacute;s social, con la finalidad de dar oportunidades habitacionales a grupos sociales vulnerables.</p>
						<p>Dentro de esta gama de proyectos, surge <b>Las Lomas de Yura</b> , bajo la responsabilidad de <b>GPR Inmobiliaria,</b> como una alternativa de concepto novedoso para <b>favorecer a sectores de la poblaci&oacute;n con menos recursos econ&oacute;micos,</b> que aspiran a obtener su primera vivienda o de avanzar a mejores condiciones de vida.</p>
						<div id="indice2" class="section-title">
							<h2 class="title">Viviendas con caracter&iacute;sticas &uacute;nicas</h2>
						</div>
						<p>Entre otros, existen cuatro aspectos fundamentales que distinguen el trabajo realizado por GPR Inmobiliaria en las casas y departamentos que forman parte de Las Lomas de Yura:</p>
						<ul>
							<li><b>Econom&iacute;a: </b>Vivienda con precios muy econ&oacute;micos o de bajo costo.</li>
							<li><b>Acondicionamiento: </b>Poseen agua, electricidad, desagües, un puesto de estacionamiento y &aacute;reas sociales (jardiner&iacute;a).</li>
							<li><b>Sustentabilidad: </b>Son de tendencia ecol&oacute;gica y contribuyen con el medio ambiente y su entorno.</li>
							<li><b>Ahorro: </b>Por su propuesta medioambiental, permiten ahorrar agua y electricidad, disminuyendo los gastos en estos servicios.</li>
						</ul>

						<div id="indice3" class="section-title">
							<h2 class="title">Tu oportunidad: Beneficios antes del 31 de diciembre de 2020</h2>
						</div>
						<p>Adem&aacute;s de las caracter&iacute;sticas señaladas, hay otros elementos que se suman y que no <b> puedes desaprovechar</b>: El Fondo Mi Vivienda, pone a tu disposici&oacute;n todas y cada una de las facilidades que ofrecen los programas <b>Techo Propio y Nuevo Cr&eacute;dito Mi Vivienda.</b> Este &uacute;ltimo incluye los beneficios del <b>Bono Buen Pagador (BBP)</b> Sostenible que añade utilidades adicionales con <b>tasas de intereses fijas</b> muy <b>bajas</b> (5% al 7.7%) y <b> porcentajes reducidos</b> para la <b>cuota inicial</b> (7.5% al 10%).</p>
						<p>Esta <b> ocasi&oacute;n,</b> de todos los <b>beneficios</b> juntos <b>(BBP Sostenible + Tasas de intereses preferenciales),</b> tienen como <b>fecha l&iacute;mite</b> el <b> 31 de diciembre de 2020.</b></p>
						<div id="indice4" class="section-title">
							<h2 class="title">¡No puedes dejar pasar este momento! ¡Post&uacute;late Ya!</h2>
						</div>
						<p>Recuerda que las <b> “oportunidades, cuando se presentan, hay que tomarlas porque son el resultado de nuestro esfuerzo”.</b></p>
						<p>Tenemos a tu disposici&oacute;n <b>244 casas</b> y <b>40 departamentos,</b> que conforman la 1era. Etapa, estimando su <b>entrega</b> a partir del mes de <b>febrero de 2021.</b> Vis&iacute;tanos en cualquiera de nuestras tres oficinas ubicadas en Arequipa, o simplemente contacta a nuestros promotores de venta, quienes te asesorar&aacute;n y ayudar&aacute;n en todo el proceso. Cont&aacute;ctanos:</p>
						<ul>
							<li>Maria : <b> <a href="tel:+51 923 104 967">+51 923 104 967</a></b>
							</li>
							<li>Daniel : <b> <a href="tel:+51 947 327 082">+51 947 327 082</a></b>
							</li>
							<li>Lidia : <b> <a href="tel:+51 947 326 649">+51 947 326 649</a></b>
							</li>
						</ul>
						<div class="cotizar-btn">
							<span class="title">Cotiza ahora tu casa</span>
							<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>" alt="arrow-up" /></a>
						</div>
					</div><!-- /.col-md-12 -->
				</div><!-- /.col-md-12 -->
			</div><!-- /.row -->
		</div>

	</div><!-- ./row -->
	<br /><br /><br />
	<div id=cotizar class="cotizar-vivienda">
		<div class="container">
			<div class="row">
				<div class="avanze1 col-sm-4 col-md-3">
					<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div>
				<div class="col-sm-12 col-md-9">
					<!-- ===================== 
												SEARCH 
										====================== -->
					<div class="line-style no-margin">
						<h2 class="title-grand">Cotizar Vivienda</h2>
					</div>
					<div class="right-box no-margin">
						<div class="row">
							<?php require(__DIR__ . "/../include/form-cotizar.php"); ?>
						</div><!-- ./row 2 -->
					</div><!-- ./search -->

				</div>
				<div class="avanze2 col-sm-4 col-md-3">
					<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div><!-- /.col-md-12 -->
			</div>
			<!--row-->
		</div>
	</div>
	<br /><br /><br />
	<div class="container">
		<div class="section-title line-style no-margin">
			<h2 class="title">Elige tu nuevo hogar</h2>
		</div>

		<div class="my-property" data-navigation=".my-property-nav">
			<div class="crsl-wrap">
			<?php require(__DIR__ . "/../include/grid-propiedades.php"); ?>
			</div>
			<div class="my-property-nav">
				<p class="button-container">
					<a href="#" class="next">siguiente</a>
					<a href="#" class="previous">anterior</a>
				</p>
			</div>
		</div><!-- /.my-property slide -->

	</div><!-- ./container -->
</section><!-- /#about-us -->

<?php require(__DIR__ ."/../include/footer2.php"); ?>