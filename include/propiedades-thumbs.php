
						<?php
						if( !empty($_REQUEST['vivienda']) and $_REQUEST['vivienda'] != "misti" ){	
							?>						
							<div class="box-ads box-grid mini">
								<a class="hover-effect image image-fill" href="propiedad-detalle?vivienda=misti">
									<span class="cover"></span>
									<img alt="Vivienda" src="/images/misti_408x251.jpg">
									<h3 class="title">Casa Misti 93 m<sup>2</sup></h3>
								</a>
								<div class="price">
									<span>Separalo con tu inicial S/ <?= GPR_PRICE_MISTI_SEPARACION ?></span>
									<a class="btn btn-default" href="propiedad/casa-misti">Cotizar</a>
								</div>						
							</div><!-- /.box-ads -->
							<?php
						}//end if					
						?>

						<?php
						if( !empty($_REQUEST['vivienda']) and $_REQUEST['vivienda'] != "aleli" ){
							?>
							<div class="box-ads box-grid mini">
								<a class="hover-effect image image-fill" href="propiedad/casa-aleli">
									<span class="cover"></span>
									<img alt="Vivienda" src="images/aleli_408x251.jpg">
									<h3 class="title">Casa Aleli 78 m<sup>2</sup></h3>
								</a>
								<div class="price">
									<span>Separalo con tu inicial S/ <?= GPR_PRICE_ALELI_SEPARACION ?></span>
									<a class="btn btn-default" href="propiedad/casa-aleli">Cotizar</a>
								</div>						
							</div><!-- /.box-ads -->
							<?php
						}//end if					
						?>

						<?php
						if( !empty($_REQUEST['vivienda']) and $_REQUEST['vivienda'] != "capuli" ){
							?>
							<div class="box-ads box-grid mini">
								<a class="hover-effect image image-fill" href="propiedad/casa-capuli">
									<span class="cover"></span>
									<img alt="Vivienda" src="images/capuli_408x251.jpg">
									<h3 class="title">Casa Capuli 42 m<sup>2</sup></h3>
								</a>
								<div class="price">
									<span>Separalo con tu inicial desde S/ <?= GPR_PRICE_CAPULI_SEPARACION ?></span>						
									<a class="btn btn-default" href="propiedad/casa-capuli">Cotizar</a>
								</div>
							</div><!-- /.box-ads -->							
							<?php
						}//end if					
						?>

						<?php
						if( !empty($_REQUEST['vivienda']) and $_REQUEST['vivienda'] != "texao" ){
							?>							
							<div class="box-ads box-grid mini">
								<a class="hover-effect image image-fill" href="propiedad/casa-texao">
									<span class="cover"></span>
									<img alt="Vivienda" src="images/texao_408x251.jpg">
									<h3 class="title">Casa Texao 35 m<sup>2</sup></h3>
								</a>
								<div class="price">
									<span>Separalo con tu inicial desde S/ <?= GPR_PRICE_TEXAO_SEPARACION ?></span>						
									<a class="btn btn-default" href="propiedad/casa-texao">Cotizar</a>
								</div>
							</div><!-- /.box-ads -->							
							<?php
						}//end if					
						?>
						
						<?php
						if( !empty($_REQUEST['vivienda']) and $_REQUEST['vivienda'] != "wititi" ){
							?>							
							<div class="box-ads box-grid mini">
								<a class="hover-effect image image-fill" href="propiedad/departamento-wititi">
									<span class="cover"></span>
									<img alt="Vivienda" src="images/wititi_408x251.jpg">
									<h3 class="title">Dpto. Wititi 53 m<sup>2</sup></h3>
								</a>
								<div class="price">
									<span>Separalo con tu inicial desde S/ <?= GPR_PRICE_WITITI_SEPARACION ?></span>						
									<a class="btn btn-default" href="propiedad/departamento-wititi">Cotizar</a>
								</div>
							</div><!-- /.box-ads -->							
							<?php
						}//end if					
						?>
						
						<?php
						if( !empty($_REQUEST['vivienda']) and $_REQUEST['vivienda'] != "yaravi" ){
							?>
							<div class="box-ads box-grid mini">
								<a class="hover-effect image image-fill" href="propiedad/departamento-yaravi">
									<span class="cover"></span>
									<img alt="Vivienda" src="images/yaravi_408x251.jpg">
									<h3 class="title">Dpto. Yaravi 46 m<sup>2</sup></h3>
								</a>
								<div class="price">
									<span>Separalo con tu inicial desde S/ 105,000</span>						
									<a class="btn btn-default" href="propiedad/departamento-yaravi">Cotizar</a>
								</div>
							</div><!-- /.box-ads -->							
							<?php
						}//end if					
						?>
						
						