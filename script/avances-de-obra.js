jQuery(document).ready(function(){
  
    jQuery('.slick-slider').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'ease',
        autoplay:true,
        autoplayspped:3000,
        customPaging : function(slider, i) {
            var slide = slider.$slides[i];
            //console.log(slide);
            var pagination = jQuery(slide).children().children(".slide").data("title");
            //console.log(pagination);
            return '<div>' + pagination + '</div>';
        },
    });

    jQuery('.slick-slider2').slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'ease',
        autoplay:true,
        autoplayspped:3000,
    });

  });