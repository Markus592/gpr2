<?php

/*
  |--------------------------------------------------------------------------
  | Mailer                                                                  |
  |--------------------------------------------------------------------------
  |                                                                         |
  | These module ares used when sending email from contact form             |
  |                                                                         |
  |--------------------------------------------------------------------------
*/

	// Configuration
	$email_site = "gprperu2019@gmail.com, gpr@sistema.cl";	
	$email_admin = "ventas@gprperu.com";
		
		$proyecto = '';
		$vivienda = '';
		$nombre = '';
		$apellido = '';
		$email = '';
		$telefono = '';
		$tipodocumento = '';
		$nrodocumento = '';
		$ciudad = '';
		$mensaje = '';
		//$subject = '';
		$user_ip = '';
		$headers = '';
		$text = '';

		// Datamail
		$proyecto = trim(stripslashes( $_POST['proyecto'] ));
		$vivienda = trim(stripslashes( $_POST['vivienda'] ));
		$nombre = trim(stripslashes( $_POST['nombre'] ));
		$apellido = trim(stripslashes( $_POST['apellido'] ));
		$email = trim(strtolower( $_POST['email'] ));
		$telefono = trim(stripslashes( $_POST['telefono'] ));
		$tipodocumento = trim(stripslashes( $_POST['tipodocumento'] ));
		$nrodocumento = trim(stripslashes( $_POST['nrodocumento'] ));
		$ciudad = trim(stripslashes( $_POST['ciudad'] ));
		$mensaje = trim(stripslashes( nl2br( $_POST['mensaje'] ) ));
		//$subject = stripslashes( $_POST['subject'] );
		$user_ip = $_SERVER['REMOTE_ADDR'];

		// Field Control
		if ( empty( $vivienda ) || empty( $nombre ) || empty( $apellido ) || empty( $email ) || empty( $telefono ) || empty( $tipodocumento ) || empty( $nrodocumento ) || empty( $ciudad ) ){

			print( 'Por favor, rellene todos los campos en el formulario.' );
			echo "<pre>";
			print_r($_POST);
			echo "</pre>";
			exit();

		}

		// Email Control
		if ( !preg_match( "/^[a-z0-9_\.\-]+@[a-z0-9\-\.]+\.[a-z]{2,4}$/", $email ) ) {

			print( 'El correo no es válido.' );
			echo "<pre>";
			print_r($_POST);
			echo "</pre>";
			exit();

		}

			// Header
			$headers = "MIME-Version: 1.0\n";
			$headers.= "X-Sender: <" . $email_admin . ">\n";
			$headers.= "X-Mailer: PHP\n";
			$headers.= "Content-type: text/html; charset=utf-8\n";
			$headers.= "From: " . $nombre . " <" . $email_admin . ">\n";
			$headers.= "Return-Path: <" . $email_admin . ">\n";
			$headers.= "Reply-To: <" . $email_admin . ">\n";

			// Body
			$text  = 'PROYECTO: <strong>'. $proyecto . '</strong><br />';			
			$text .= '------------------------------<br />';
			$text .= 'Nombres: <strong>'. $nombre . '</strong><br />';
			$text .= 'Apellidos: <strong>'. $apellido . '</strong><br />';
			$text .= 'Email: <strong>'. $email .'</strong><br />';
			$text .= 'Teléfono: <strong>'. $telefono . '</strong><br />';
			$text .= 'Tipo de documento: <strong>'. $tipodocumento . '</strong><br />';
			$text .= 'Nro. de documento: <strong>'. $nrodocumento . '</strong><br />';
			$text .= 'Ciudad: <strong>'. $ciudad . '</strong><br />';
			$text .= 'Cotizar: <strong>'. $vivienda . '</strong><br />';
			$text .= '------------------------------<br />';
			$text .= '<strong>' . $mensaje . '</strong><br />';
			$text .= '------------------------------<br /><br />';
			$text .= 'IP:  <strong>'. $user_ip . '</strong>';

			$subject = "GPR / Cotizar: ". $vivienda;
			
		// Send email
		if ( @mail ( $email_site, $subject, $text, $headers ) ){

			header("Location:gracias.php?send=ok");
			
			print( "Mensaje enviado con éxito." );
			exit;

		} else {

			print( "Mensaje no pudo ser enviado." );
			exit;

		}