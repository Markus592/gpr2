<?php require '../include/config.php'; ?>
<?php
define('og_image', 'images/visita-guiada.jpg');
define('og_title', 'GPR Inmobiliaria Arequipa - Contacto');
define('og_type','website');
define('og_desc','Oficina Interior Municipalidad de Yura: Arequipa (Referencia: Km. 15.5 Pasando Colegio Solaris, Carretera Arequipa - Yura).
Teléfono: 947 326 649
Horario de atención: De Lunes a S&aacutebado; de 8a.m a 1p.m, los sábados visita nuestro terreno de 02:30 p.m a 04:15 p.m. y domingos de 09:30 a.m a 5:00 p.m.');
define('keywords','venta Mini Departamento,Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
define('title','Oficina Interior Municipalidad de Yura '.GPR_TITLE_GENERAL);
define('GPR_SECTION_CLASS','oficina-terminalpesquero');
?>
<?php require '../include/header.php'; ?>
		<section class="header-margin-base">
			<!--<div id="map-canvas" class="header-map"></div>-->
			<div id="breadcrumb">
				<div class="container">
					<ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-home"></i></a></li>
						<li><a href="#">Inicio</a></li>
						<li class="active">Contacto</li>
					</ol>
				</div>
			</div><!-- /#breadcrumb -->
			<span class="cover"></span>
		</section><!-- /#header -->
		
		<section id="property-content" style="padding-top:30px;">
			<div class="container">
				<div class="row">
					<div class="col-md-8">

						<div class="title">
							<h1>Oficina Municipalidad Yura</h1>
						</div>

						<div class="row">
							<div class="col-md-12">							
								<div class="section-title line-style">
									<h2 class="title">Visítanos</h2>
								</div>
								<p class="panel-heading">No esperes más y ven a visitarnos a nuestras oficinas de venta:</p>							
								
								<p><b>Oficina Interior Municipalidad de Yura:</b> Arequipa (Referencia: Km. 15.5 Pasando Colegio Solaris, Carretera Arequipa - Yura).</p>
								<p><b>Teléfono:</b><a class="gpr-whatsapp" target="_blank"  href="https://api.whatsapp.com/send?phone=51947326649&text=Hola,%20deseo%20informaci%C3%B3n%20de%20las%20Lomas%20de%20Yura"><i class="fa fa-whatsapp"> </i> 947 326 649</a></p>
								<p><b>Horario de atención:</b>De Lunes a S&aacutebado; de 8a.m a 1p.m</p>						
								<p><b><i class="fa fa-map-marker"> </i> Google Map: </b><a  target="_blank" href= https://www.google.com/maps/search/?api=1&query=-16.3291284,-71.6782146>Oficina Municipalidad de Yura</a></p>						
							</div>
						</div>
						
					</div>
					<div class="col-md-4">
						<?php require '../include/visita-guiada.php'; ?>						
					</div>
				</div>
			</div>
		</section>
		

<?php require '../include/footer2.php'; ?>